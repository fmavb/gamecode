// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "CompactLoginSystemType.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CompactLoginSystemUtil.generated.h"

class ACompactLoginSystemMgr;

UCLASS()
class UCompactLoginSystemUtil : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static TSharedRef<IHttpRequest> GetRequest(FString HostAddress);
	
	static TSharedRef<IHttpRequest> PostRequest(FString HostAddress, FString ContentJson);

	static void SetHeaders(TSharedRef<IHttpRequest>& Request);

	//Customized Json
	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FCustomizedJson MakeStringJson(FString Key, FString Value);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FCustomizedJson MakeIntegerJson(FString Key, int32 Value);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FCustomizedJson MakeFloatJson(FString Key, float Value);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FCustomizedJson MakeBooleanJson(FString Key, bool Value);

	//do not support iteratively json object
	static TSharedPtr<FJsonValue> GetJsonValue(FCustomizedJson Json, FString Key);

	//input should top json object
	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FString GetStringValueFromJson(FCustomizedJson Json, FString Key);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static int32 GetIntegerValueFromJson(FCustomizedJson Json, FString Key);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static float GetFloatValueFromJson(FCustomizedJson Json, FString Key);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static bool GetBooleanValueFromJson(FCustomizedJson Json, FString Key);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FString ConvertJsonArrayToString(TArray<FCustomizedJson> JsonArray);

	UFUNCTION(BlueprintPure, Category = "CompactLoginSystemUtil")
	static FCustomizedJson ConvertStringToJson(FString JsonString);
};