// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CompactLoginSystemMgr.h"
#include "CompactLoginSystemUtil.h"
#include "Kismet/GameplayStatics.h"
//////////////////////////////////////////////////////////////////////////
// CompactLoginSystemMgr
////////////////////////////////////////////////////////////////////
ACompactLoginSystemMgr::ACompactLoginSystemMgr(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void ACompactLoginSystemMgr::BeginPlay()
{
	Super::BeginPlay();
}

ACompactLoginSystemMgr* ACompactLoginSystemMgr::GetLoginSystemManager(UObject* WorldContentObject)
{
//	if (LoginMgrRef.IsValid())
//	{
//		return LoginMgrRef.Get();
//	}
	ACompactLoginSystemMgr* LoginMgr = nullptr;
	if (WorldContentObject && WorldContentObject->GetWorld())
	{
		TArray<AActor*> LoginMgrList;
		UGameplayStatics::GetAllActorsOfClass(WorldContentObject, ACompactLoginSystemMgr::StaticClass(), LoginMgrList);
		if (LoginMgrList.Num() > 0)
		{
			LoginMgr = Cast<ACompactLoginSystemMgr>(LoginMgrList[0]);
		}
	}
	if (LoginMgr == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("GetLoginSystemManager fail, manager is null."));
	}
//	LoginMgrRef = LoginMgr;	
	return LoginMgr;
}

void ACompactLoginSystemMgr::SendClientRequest(FString ServerAddress, TArray<FCustomizedJson> RequestJsonArray, const FOnServerResponseDelegate& ResponseCallback)
{
	FString ContentJson = UCompactLoginSystemUtil::ConvertJsonArrayToString(RequestJsonArray);
	FHttpRequestPtr Request = UCompactLoginSystemUtil::PostRequest(ServerAddress, ContentJson);
	if (!ResponseDelegateMap.Contains(Request))
	{
		ResponseDelegateMap.Add(Request, ResponseCallback);
	}
	else
	{
		ResponseDelegateMap[Request] = ResponseCallback;
	}
	Request->OnProcessRequestComplete().BindUObject(this, &ACompactLoginSystemMgr::WebServerResponse);
	Request->ProcessRequest();
}

void ACompactLoginSystemMgr::WebServerResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	FString ReturnJsonStr;
	FCustomizedJson ParsedJson;
	ParsedJson.bIsValid = false;
	if (bWasSuccessful)
	{
		ReturnJsonStr = Response->GetContentAsString();
		ParsedJson = UCompactLoginSystemUtil::ConvertStringToJson(ReturnJsonStr);
		ParsedJson.bIsValid = true;
	}
	if (ResponseDelegateMap.Contains(Request))
	{
		ResponseDelegateMap[Request].ExecuteIfBound(ParsedJson, ReturnJsonStr);
	}
}