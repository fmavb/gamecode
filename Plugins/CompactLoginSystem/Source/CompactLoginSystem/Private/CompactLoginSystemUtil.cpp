// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "CompactLoginSystemUtil.h"
#include "Serialization/JsonSerializer.h"

void UCompactLoginSystemUtil::SetHeaders(TSharedRef<IHttpRequest>& Request)
{
	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
}

TSharedRef<IHttpRequest> UCompactLoginSystemUtil::GetRequest(FString HostAddress)
{
	TSharedRef<IHttpRequest> Request = FHttpModule::Get().CreateRequest();
	Request->SetURL(HostAddress);
	SetHeaders(Request);
	Request->SetVerb("GET");
	return Request;
}

TSharedRef<IHttpRequest> UCompactLoginSystemUtil::PostRequest(FString HostAddress, FString ContentJson)
{
	TSharedRef<IHttpRequest> Request = FHttpModule::Get().CreateRequest();
	Request->SetURL(HostAddress);
	SetHeaders(Request);
	Request->SetVerb("POST");
	Request->SetContentAsString(ContentJson);
	return Request;
}


//Customized Json
FCustomizedJson UCompactLoginSystemUtil::MakeStringJson(FString Key, FString Value)
{
	FCustomizedJson Json;
	Json.NativeJsonKey = Key;
	Json.NativeJsonValue = MakeShareable(new FJsonValueString(Value));	
	return Json;
}

FCustomizedJson UCompactLoginSystemUtil::MakeIntegerJson(FString Key, int32 Value)
{
	FCustomizedJson Json;
	Json.NativeJsonKey = Key;
	Json.NativeJsonValue = MakeShareable(new FJsonValueNumber((double)Value));
	return Json;
}

FCustomizedJson UCompactLoginSystemUtil::MakeFloatJson(FString Key, float Value)
{
	FCustomizedJson Json;
	Json.NativeJsonKey = Key;
	Json.NativeJsonValue = MakeShareable(new FJsonValueNumber(Value));
	return Json;
}

FCustomizedJson UCompactLoginSystemUtil::MakeBooleanJson(FString Key, bool Value)
{
	FCustomizedJson Json;
	Json.NativeJsonKey = Key;
	Json.NativeJsonValue = MakeShareable(new FJsonValueBoolean(Value));
	return Json;
}

TSharedPtr<FJsonValue> UCompactLoginSystemUtil::GetJsonValue(FCustomizedJson Json, FString Key)
{
	TSharedPtr<FJsonValue> OutJsonValue;
	if (Json.NativeJsonValue.IsValid() && Json.NativeJsonValue->Type == EJson::Object)
	{
		const TSharedPtr<FJsonObject>& JsonObj = Json.NativeJsonValue->AsObject();
		if (JsonObj.IsValid() && JsonObj->Values.Contains(Key))
		{
			return JsonObj->Values[Key];
		}
	}
	return OutJsonValue;
}

FString UCompactLoginSystemUtil::GetStringValueFromJson(FCustomizedJson Json, FString Key)
{
	TSharedPtr<FJsonValue> JsonValue = GetJsonValue(Json, Key);
	if (JsonValue.IsValid() && JsonValue->Type == EJson::String)
	{
		return JsonValue->AsString();
	}
	return TEXT("");
}

int32 UCompactLoginSystemUtil::GetIntegerValueFromJson(FCustomizedJson Json, FString Key)
{
	TSharedPtr<FJsonValue> JsonValue = GetJsonValue(Json, Key);
	if (JsonValue.IsValid() && JsonValue->Type == EJson::Number)
	{
		return (int32)(JsonValue->AsNumber());
	}
	return 0;
}

float UCompactLoginSystemUtil::GetFloatValueFromJson(FCustomizedJson Json, FString Key)
{
	TSharedPtr<FJsonValue> JsonValue = GetJsonValue(Json, Key);
	if (JsonValue.IsValid() && JsonValue->Type == EJson::Number)
	{
		return (float)(JsonValue->AsNumber());
	}
	return 0.f;
}

bool UCompactLoginSystemUtil::GetBooleanValueFromJson(FCustomizedJson Json, FString Key)
{
	TSharedPtr<FJsonValue> JsonValue = GetJsonValue(Json, Key);
	if (JsonValue.IsValid() && JsonValue->Type == EJson::Boolean)
	{
		return JsonValue->AsBool();
	}
	return false;
}

FString UCompactLoginSystemUtil::ConvertJsonArrayToString(TArray<FCustomizedJson> JsonArray)
{
	
	TSharedPtr<FJsonObject> JsonObj = MakeShareable(new FJsonObject);
	
	for (auto Json : JsonArray) 
	{
		if (Json.NativeJsonValue.IsValid())
		{
			JsonObj->SetField(Json.NativeJsonKey, Json.NativeJsonValue);
		}
	}

	FString JsonStr;
	auto JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);
	FJsonSerializer::Serialize(JsonObj.ToSharedRef(), JsonWriter);;

	return 	JsonStr;
}

FCustomizedJson UCompactLoginSystemUtil::ConvertStringToJson(FString JsonString)
{
	TSharedPtr<FJsonValue> JsonValue;
	TSharedRef< TJsonReader<TCHAR> > JsonReader = TJsonReaderFactory<TCHAR>::Create(JsonString);
	FJsonSerializer::Deserialize(JsonReader, JsonValue);
	FCustomizedJson OutJson;
	OutJson.NativeJsonValue = JsonValue;
	OutJson.bIsValid = true;
	return OutJson;
}