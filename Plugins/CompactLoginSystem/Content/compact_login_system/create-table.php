<?php

$sql =<<<EOF
CREATE TABLE users (id INT PRIMARY KEY     NOT NULL, username           TEXT    NOT NULL, password            TEXT     NOT NULL, name           TEXT    NOT NULL, email        TEXT     NOT NULL, phone        TEXT     NOT NULL, role        TEXT     NOT NULL, active        TEXT     NOT NULL, last         TEXT     NOT NULL);
CREATE TABLE auth (email TEXT PRIMARY KEY  NOT NULL, authcode TEXT NOT NULL, timestamp INTEGER NOT NULL);
EOF;

?>
