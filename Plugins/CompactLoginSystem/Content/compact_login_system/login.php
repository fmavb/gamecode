<?php

  // Get the conent
  $post_data = file_get_contents('php://input');

  // decode json
  $obj = json_decode($post_data);

  // user name or email
  //$email = $obj->{'email'};
  $username = $obj->{'username'};

  // password, encoded using base64
  $password = base64_encode($obj->{'password'}); 

  // login result
  $loginresult = FALSE;

  // connects to dabtabase
  $db = new PDO("sqlite:" . "databases/UserData.db");

  // user name check
  $userfound = FALSE;    
  $query = "SELECT * FROM users WHERE username = '$username'";
  foreach($db->query($query) as $data) 
  {  
      $userfound = TRUE;
      //check password
      if($password == $data['password']) 
      {
        // update last login time of the user
        // Set date.timezone in php.ini or call function date_default_timezone_set('UTC')
        $last = date('Y-m-d H:i:s'); //date("g:i A, j F o", time());
        $sql = "UPDATE users SET last = '$last' WHERE username='$username'"; 
        $db->exec($sql);
        $loginresult = TRUE;
      }  
  }

  //email check
  if(!$userfound)
  {     
      $query = "SELECT * FROM users WHERE username = '$username'";
      foreach($db->query($query) as $data) 
      {
          $userfound = TRUE;
          //check password
          if($password == $data['password']) 
          {
            // update last login time of the user
            $last = date('Y-m-d H:i:s'); //$last = date("g:i A, j F o", time());
            $sql = "UPDATE users SET last = '$last' WHERE username='$username'"; 
            $db->exec($sql);
            $loginresult = TRUE;
          } 
      }
  }
  
  $error = 'Login Successful!';
  $retcode = 0;
  if(!$userfound)
  {
    $retcode = 1;
    $error = 'User not found, please check username or register an account!';
  }
  else if(!$loginresult)
  {
    $retcode = 2;
    $error = 'Password is wrong!';
  }

  require('util.php');
  $rep = new ClientReqResp();
  $rep->retcode = $retcode;
  $rep->message = $error;

  echo json_encode($rep);
?>